/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Des Salzinnois se bougent, bougez-vous pour Salzinnes',
  tagline: 'Présentation de deux projets citoyens centrés sur Salzinnes dans le cadre du budget participatif de la Ville de Namur - version 2023',
  url: 'https://slzdm.gitlab.io',
  baseUrl: '/projpart-2023-doc/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'Salzinnes Demain', // Usually your GitHub org/user name.
  projectName: 'Projets Participatifs Salzinnois 2023', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Projets Participatifs Salzinnois 2023',
      logo: {
        alt: 'My Site Logo',
        src: 'img/image.png',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Infos générales',
          position: 'left',
        },
        {to: 'blog', label: 'Infos précises sur les projets présentés', position: 'left'},
        {
          href: 'https://gitlab.com/SlzDM/projpart-2023-doc',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Infos complémentaires',
          items: [
            {
              label: 'Salzinnes Demain ASBL',
              to: 'docs/',
            },
          ],
        },
        // {
        //   title: 'Community',
        //   items: [
        //     {
        //       label: 'Stack Overflow',
        //       href: 'https://stackoverflow.com/questions/tagged/docusaurus',
        //     },
        //     {
        //       label: 'Discord',
        //       href: 'https://discordapp.com/invite/docusaurus',
        //     },
        //     {
        //       label: 'Twitter',
        //       href: 'https://twitter.com/docusaurus',
        //     },
        //   ],
        // } ,
        {
          title: 'Pour en savoir davantage',
          items: [
            {
              label: 'Blog',
              to: 'blog',
            },
            // {
            //   label: 'GitHub',
            //   href: 'https://github.com/facebook/docusaurus',
            // },
          ],
        },
      ],
	copyright: `Copyright © ${new Date().getFullYear()} Projets Participatifs Salzinnois 2023, Salzinnes Demain, Webdev FrdVnW. Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
