---
title: Salzinnes Demain ASBL
slug: /
---
« La façon dont nous collaborons, fonctionnons et réalisons nos projets est aussi importante que l'action en tant que telle. »

## Raison d’être :

Le Collectif Salzinnes Demain vise à mobiliser toutes les forces vives autour d’une démarche citoyenne participative, collaborative, constructive et solidaire axée sur le vivre ensemble.

## Historique :

Début 2016, confrontés à des projets immobiliers susceptibles de modifier sensiblement leur milieu de vie, des salzinnois, le comité de quartier Henri Lemaitre, le comité de quartier Kegeljan et le comité de quartier Salzinnes-Écoles, rejoints par le comité de quartier des Balances et l’association des commerçants de Salzinnes, se sont associés au sein d’un Collectif pour se positionner comme interlocuteur vis-à-vis des acteurs, promoteurs et autorités. Rapidement, ce groupe s’est donné le nom de « Collectif Salzinnes Demain ».

En effet, Salzinnes représente plus de 8.400 habitants. Il accueille sur son territoire un nombre importants d’acteurs et d’opérateurs privés et publics assurant des activités de service public, de commerce, de soins hospitaliers, d’institutions scolaires et estudiantines, sans oublier une présence importante de l’associatif.

Après avoir dressé un état de lieux présenté en octobre 2016, le Collectif a réuni les habitants pour un brainstorming destiné à recueillir un maximum d’idées. Petit à petit, certaines d’entre elles ont été initiées sous forme de fiche-action alors que les contacts se multipliaient avec la Ville, la Province, le Foyer Namurois, …. 

En 2017, le jeune comité de quartier Salzinnes-les-Hauts a rejoint le collectif, suivi en 2018 par celui des « Voisins de la Colline ».

En janvier 2018, le Collège Communal au complet a présenté à un public nombreux les grands chantiers prévus pour notre faubourg. Des groupes de travail mixtes, administration communale/ Salzinnes Demain, se mettent en place dans l’optique d’associer les salzinnois aux travaux des fonctionnaires pour une construction conjointe de l’avenir du faubourg.

Fin 2018, lors de sa rencontre avec le bourgmestre M.Prévot, le Collectif s’est encore affirmé davantage comme interlocuteur privilégié vis-à-vis des autorités communales.

## Objectifs :

Dans l’espoir de contribuer au maintien des qualités durables pré-existantes du faubourg et de développer celles qui ne sont pas encore mises en œuvre, le Collectif se propose d’intervenir de façon autonome ou en tant qu’interlocuteur vis à vis des pouvoirs publics, des intervenants économiques et des associations, de soutenir, d’organiser et de favoriser toute action, événement ou projet visant à promouvoir le concept de développement durable et de transition avec la participation active et citoyenne des habitants. Dans cette optique, il veille aussi à la sauvegarde d’une vie de qualité sous ses divers aspects (santé, sécurité, liberté de circulation, partage de l’espace public, convivialité, entraide, solidarité, citoyenneté, responsabilité …) .

Par quartier de Salzinnes nous entendons : la zone géographique délimitée par le méandre de la Sambre à l'Ouest, au Nord et à l'Est et la colline de la citadelle au Sud-Est. La Sambre est une limite nette, alors que la Citadelle est plus perméable. Nous limiterons néanmoins le faubourg à la rue des Noyers. Les points d'entrée Ouest et Est sont la "Gueule du Loup" et le Pont de l'Evêché.

Parce que le quartier est l’échelle d’action pertinente pour aborder les problématiques telles que l’étalement urbain, la mobilité, la pollution, la dépendance énergétique, les défis sociaux et démographiques, la place de la nature en ville … le Collectif souhaite y porter un regard sous l’angle des critères repris dans le référentiel « quartiers durables » et dans une perspective de Transition afin que leur milieu de vie devienne plus résilient, plus soutenable et plus agréable à vivre.

## Info complémentaires ...
... sur le site [https://salzinnes-demain.org/](https://salzinnes-demain.org/)
